var $j = jQuery.noConflict();

$j(function($){
    /* tune some customer requests, disabling some button */
    $("#taal :input").attr("disabled", true);
    // $("#wanneer :input").first().attr('disabled', true);
    // $("#wanneer > .first > label").append(
    //     "<span data-balloon-length=\'medium\' class=\'balloon\' aria-label=\'Zit er spoed bij? Stuur een bericht en ik kijk met je mee naar de mogelijkheden\' data-balloon-pos=\'up\'>&#9432;</span>"
    // );
 
    /* get the default value of the number of words 
        and append it to the empty span
    */
    let aantal = $("#aantal").val();
    $("#woorden").append(aantal);
        
    /* if the input change, change the outcome on the fly */

    /* when we change the slider of words */
    $("#aantal").change(function() {
        aantal = $("#aantal").val();
        $("#woorden").html(aantal);
        calculate();
    })

    /* when we select checkbox structuur */
    $("#structuur :input").change(function() {
        calculate();
    })

    /* when we select checkbox taal */
    $("#taal :input").change(function() {
        console.log('changing');
        calculate();
    })

    /* when we select checkbox begrijpelijkheid */
    $("#begrijpelijkheid :input").change(function() {
        calculate();
    })

    /* when we choose another radio option for delivery time */
    $("#wanneer :input").change(function() {
        calculate();
    });

    /* and just calculate with the initial values */
    calculate();

    /* here we do the calculation */
    function calculate() {
        console.log('calculating');
        $("#prijsindicatie").empty();
        let ppw = 0.04;
        let waar = 0;

        /* if taal and 3 days => if begrijpelijkheid or structuur*/
        if ($("#taal :input").prop("checked") == true && $("input[name='radio-wanneer']:checked").val() == "Over drie dagen") {
            if ($("#begrijpelijkheid :input").prop("checked") == true && $("#structuur :input").prop("checked") == true) {
                ppw = 0.06;
            }
            else if ($("#begrijpelijkheid :input").prop("checked") == true || $("#structuur :input").prop("checked") == true) {
                    ppw = 0.05;
            }
            else {
                ppw = 0.04;
            }
        }
                
        /* if taal and 1 week => if begrijpelijkheid or structuur*/
        else if ($("#taal :input").prop("checked") == true && $("input[name='radio-wanneer']:checked").val() == "Over een week") {
            if ($("#begrijpelijkheid :input").prop("checked") == true && $("#structuur :input").prop("checked") == true) {
                ppw = 0.045;
            }
            else if (($("#begrijpelijkheid :input").prop("checked") == true) || ($("#structuur :input").prop("checked") == true)) {
                ppw = 0.035;
            }
            else {
                ppw = 0.03;
            }
        }

        /* if taal + 1 month => if begrijpelijkheid or structuur*/
        else if ($("#taal :input").prop("checked") == true && $("input[name='radio-wanneer']:checked").val() == "Over een maand") {
            if ($("#begrijpelijkheid :input").prop("checked") == true && $("#structuur :input").prop("checked") == true) {
                ppw = 0.035;
            }
            else if ($("#begrijpelijkheid :input").prop("checked") == true || $("#structuur :input").prop("checked") == true) {
                    ppw = 0.03;
            }
            else {
                ppw = 0.025;
            }
        }
        

        let indicatie = (aantal * ppw) + 15;
        let minind = indicatie - (indicatie * 0.15);
        let maxind = indicatie + (indicatie * 0.15); 

        $("#prijsindicatie").append("Prijsindicatie: <span>&euro;" + Math.round(minind) + " - &euro;" + Math.round(maxind) + "</span>");
        $("#prijsindicatie").css("display" , "block");


    } /* end of calc function */





    $("#contact").click(function() {
        $("#MyPopup").css("width" , "360px" );
        $("#contactform").toggle();
        $("#close").toggle();   
    })
    $("#close").click(function() {
        $("#MyPopup").css("width" , "160px" );
        $("#contactform").toggle();
        $("#close").toggle(); 
    })

    // When the user clicks on the button, open the mobile menu modal
    $("#openMobileMenu").click(function() {
        console.log("clicking");
        $("#mobileMenu").css("display", "flex");
    });

    // When the user clicks on <span> (x), close the modal
    $(".close").click(function() {
       $("#mobileMenu").css("display", "none"); 
    });
    $(".menu-item").click(function() {
        $("#mobileMenu").css("display", "none");
    })
        
    // fixed header 
    $("#primary").css("margin-top", "220px");

    $(window).on('scroll', function() {
        if($(document).scrollTop() > 50 ) {
            $('.site-branding').css('display', 'none');
            $('#imglogosmall').css('display', 'block');
            $('#homeicon').css('display', 'none');
        } else {
            $('.site-branding').css('display', 'block');
            $('#imglogosmall').css('display', 'none');
            $('#homeicon').css('display', 'block');

        }
    });
    

});
